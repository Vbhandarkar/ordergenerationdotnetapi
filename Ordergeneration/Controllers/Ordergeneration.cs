using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Hosting;


namespace Ordergeneration.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdergenerationController : ControllerBase
    {
        private IHostingEnvironment _env;
        public OrdergenerationController(IHostingEnvironment env)
        {
            _env = env;
        }


        // POST api/values
        [HttpPost]
        public String Post([FromBody] OrderGenVM orderGenVM)
        {

            String runId = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");

            List<MyPair[]> orderList = GenerateOrder.generate(orderGenVM.noOfBatch, orderGenVM.orderSeq,
                                          orderGenVM.orderCtr, new ArrayList(orderGenVM.itemQtyArray),
                                          orderGenVM.batchSize, orderGenVM.pL1Q1, orderGenVM.pL1Q2,
                                          orderGenVM.pL2Q1, orderGenVM.pL3Q1, orderGenVM.pL4Q1, orderGenVM.path);

            List<Audit> auditList = new List<Audit>();

            foreach (MyPair[] myPairs in orderList)
            {

                String error = null;
                String output = null;
                DateTime startTime = DateTime.Now;
                DateTime endTime;

                try
                {
                    output = GenerateOrder.createOrder(myPairs);
                }
                catch (Exception e)
                {
                    error = e.ToString();
                }
                endTime = DateTime.Now;
                Audit audit = new Audit(error, output, startTime, endTime, myPairs.ToString(), orderGenVM.ToString(), endTime.Subtract(startTime).TotalMilliseconds);
                auditList.Add(audit);

            }

            string jsonData = JsonConvert.SerializeObject(auditList, Formatting.None);
            var webRoot = _env.WebRootPath;
            var file = System.IO.Path.Combine(webRoot, runId + ".txt");
            System.IO.File.WriteAllText(file, jsonData);
            return runId;
        }

        [HttpPost("{methodName}")]
        public List<MyPair[]> postTest([FromBody] OrderGenVM orderGenVM)
        {
            return GenerateOrder.generate(orderGenVM.noOfBatch, orderGenVM.orderSeq,
                                          orderGenVM.orderCtr, new ArrayList(orderGenVM.itemQtyArray),
                                          orderGenVM.batchSize, orderGenVM.pL1Q1, orderGenVM.pL1Q2,
                                          orderGenVM.pL2Q1, orderGenVM.pL3Q1, orderGenVM.pL4Q1, orderGenVM.path);
        }

    }



    class GenerateOrder
    {


        static String NOP_URL = "https://demostore4.projectverte.com";

        static String NOP_Username = "orders@store.com";

        static String NOP_Password = "Verte@2019";

        static String NOP_GiftCard = "7bb4d23e-f956";

        static String NOP_VendorPrefix = "VS0025";

        public static List<MyPair[]> generate(int noOfBatch, String orderSeq, int orderCtr, ArrayList itemQtyArray,
     Hashtable batchSize, int pL1Q1, int pL1Q2, int pL2Q1, int pL3Q1, int pL4Q1, String path)
        {

            int itemCtr = 0;
            List<MyPair[]> mypairList = new List<MyPair[]>();

            for (int batch = 0; batch < noOfBatch; batch++)
            {
                int noOfOrd = orderCtr;
                int nL1Q1 = (int)Math.Ceiling(pL1Q1 * noOfOrd / 100.0);
                int nL1Q2 = (int)Math.Ceiling(pL1Q2 * noOfOrd / 100.0);
                int nL2Q1 = (int)Math.Ceiling(pL2Q1 * noOfOrd / 100.0);
                int nL3Q1 = (int)Math.Ceiling(pL3Q1 * noOfOrd / 100.0);
                int nL4Q1 = (int)Math.Ceiling(pL4Q1 * noOfOrd / 100.0);
                for (int j = 0; (j < nL1Q1) && (itemQtyArray.Count > 0); j++)
                {
                    MyPair pair = (MyPair)itemQtyArray[itemCtr];
                    pair.decrementQty();
                    if (pair.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                    //    itemQtyArray.Insert(itemCtr, pair);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }

                    mypairList.Add((new MyPair[] { new MyPair(pair.getItem(), 1) }));
                }
                for (int j = 0; (j < nL1Q2) && (itemQtyArray.Count > 0); j++)
                {
                    MyPair pair = (MyPair)itemQtyArray[itemCtr];
                    if (pair.getQty() < 2)
                    {
                        itemCtr++;
                        j--;
                    }
                    else
                    {
                        pair.decrementQty();
                        pair.decrementQty();
                        if (pair.getQty() <= 0)
                        {
                            itemQtyArray.Remove(itemCtr);
                        }
                        else
                        {
                           // itemQtyArray.Insert(itemCtr, pair);
                            itemCtr++;
                        }
                        if (itemCtr >= itemQtyArray.Count)
                        {
                            itemCtr = 0;
                        }
                        mypairList.Add((new MyPair[] { new MyPair(pair.getItem(), 2) }));
                    }
                }
                for (int j = 0; (j < nL2Q1) && (itemQtyArray.Count > 0); j++)
                {
                    MyPair pair1 = (MyPair)itemQtyArray[itemCtr];
                    pair1.decrementQty();
                    if (pair1.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                       // itemQtyArray.Insert(itemCtr, pair1);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }
                    MyPair pair2 = (MyPair)itemQtyArray[itemCtr];
                    pair2.decrementQty();
                    if (pair2.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                        //itemQtyArray.Insert(itemCtr, pair2);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }

                    mypairList.Add(new MyPair[] { new MyPair(pair1.getItem(), 1), new MyPair(pair2.getItem(), 1) });
                }
                for (int j = 0; (j < nL3Q1) && (itemQtyArray.Count > 0); j++)
                {
                    MyPair pair1 = (MyPair)itemQtyArray[itemCtr];
                    pair1.decrementQty();
                    if (pair1.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                       // itemQtyArray.Insert(itemCtr, pair1);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }
                    MyPair pair2 = (MyPair)itemQtyArray[itemCtr];
                    pair2.decrementQty();
                    if (pair2.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                        //itemQtyArray.Insert(itemCtr, pair2);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }
                    MyPair pair3 = (MyPair)itemQtyArray[itemCtr];
                    pair3.decrementQty();
                    if (pair3.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                       // itemQtyArray.Insert(itemCtr, pair3);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }
                    mypairList.Add(new MyPair[] { new MyPair(pair1.getItem(), 1), new MyPair(pair2.getItem(), 1),
                                new MyPair(pair3.getItem(), 1) });
                }
                for (int j = 0; (j < nL4Q1) && (itemQtyArray.Count > 0); j++)
                {
                    MyPair pair1 = (MyPair)itemQtyArray[itemCtr];
                    pair1.decrementQty();
                    if (pair1.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                        //itemQtyArray.Insert(itemCtr, pair1);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }
                    MyPair pair2 = (MyPair)itemQtyArray[itemCtr];
                    pair2.decrementQty();
                    if (pair2.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                        //itemQtyArray.Insert(itemCtr, pair2);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }
                    MyPair pair3 = (MyPair)itemQtyArray[itemCtr];
                    pair3.decrementQty();
                    if (pair3.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                        //itemQtyArray.Insert(itemCtr, pair3);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }
                    MyPair pair4 = (MyPair)itemQtyArray[itemCtr];
                    pair4.decrementQty();
                    if (pair4.getQty() <= 0)
                    {
                        itemQtyArray.Remove(itemCtr);
                    }
                    else
                    {
                        //itemQtyArray.Insert(itemCtr, pair4);
                        itemCtr++;
                    }
                    if (itemCtr >= itemQtyArray.Count)
                    {
                        itemCtr = 0;
                    }
                    mypairList.Add((new MyPair[] { new MyPair(pair1.getItem(), 1), new MyPair(pair2.getItem(), 1),
                            new MyPair(pair3.getItem(), 1), new MyPair(pair4.getItem(), 1) }));

                }
            }
            return mypairList;
        }



        public static string createOrder(MyPair[] myPairs)
        {
            String token = getAuthToken(null);
            var client = new RestClient(NOP_URL);

            var request = new RestRequest("api/VerteSimulation/CreateOrder", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);

            Order order = new Order();
            order.giftCard = NOP_GiftCard;
            order.email = NOP_Username;

            Address address = new Address();
            address.countryCode = "US";
            address.stateCode = "GA";
            address.city = "Sandy Springs";
            address.address1 = "8601 Dunwoody Place, Ste 200";
            address.zipPostalCode = "30350";
            address.phone = "5555555555";
            address.firstName = "Orders";
            address.lastName = "Placement";

            order.address = address;

            OrderItem[] orderItems = new OrderItem[myPairs.Length];
            for (int i = 0; i < myPairs.Length; i++)
            {
                MyPair myPair = myPairs[i];

                OrderItem orderItem = new OrderItem
                {
                    sku = myPair.item,
                    quantity = myPair.qty.ToString(),
                    vendorPrefix = NOP_VendorPrefix
                };

                orderItems[i] = orderItem;
            }

            order.orderItems = orderItems;

            string orderJson = JsonConvert.SerializeObject(order);
            request.AddJsonBody(orderJson);
            request.AddHeader("Content-Type", "application/json");

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string

            return content;
        }


        public static string getAuthToken(String emailid)
        {

            var client = new RestClient(NOP_URL);
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("/api/VerteAuth/Authenticate", Method.POST);

            Login login = new Login(NOP_Username, NOP_Password);

            string loginjson = JsonConvert.SerializeObject(login);
            request.AddJsonBody(loginjson);
            request.AddHeader("Content-Type", "application/json");

            // execute the request
            IRestResponse response = client.Execute(request);
            String content = response.Content; // raw content as string
            JObject tokenJobject = JObject.Parse(content);
            return tokenJobject["Token"].ToString();
        }


    }

    class Login
    {
        public String Username;
        public String Password;

        public Login(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }

    class Order
    {
        public String email;
        public String giftCard;
        public OrderItem[] orderItems;
        public Address address;

    }

    class OrderItem
    {
        public String sku;
        public String vendorPrefix;
        public String quantity;
    }

    class Address
    {
        public String countryCode;
        public String stateCode;
        public String city;
        public String address1;
        public String zipPostalCode;
        public String phone;
        public String firstName;
        public String lastName;
    }

    class Audit
    {
        public String error = null;
        public String output = null;
        public DateTime startTime;
        public DateTime endTime;
        public String input = null;
        public String runCOnfig = null;
        public double executiontime;

        public Audit(string error, string output, DateTime startTime, DateTime endTime, string input, string runCOnfig, double executiontime)
        {
            this.error = error;
            this.output = output;
            this.startTime = startTime;
            this.endTime = endTime;
            this.input = input;
            this.runCOnfig = runCOnfig;
            this.executiontime = executiontime;
        }
    }

    public class MyPair
    {

        public String item { get; set; }
        public int qty { get; set; }

        public MyPair(String item, int qty)
        {
            this.item = item;
            this.qty = qty;
        }

        public void decrementQty()
        {
            this.qty = qty - 1;
        }

        public int getQty()
        {
            return qty;
        }

        public String getItem()
        {
            return item;
        }
    }

    public class OrderGenVM
    {
        public int noOfBatch { get; set; }
        public String orderSeq { get; set; }
        public int orderCtr { get; set; }
        public MyPair[] itemQtyArray { get; set; }
        public Hashtable batchSize { get; set; }
        public int pL1Q1 { get; set; }
        public int pL1Q2 { get; set; }
        public int pL2Q1 { get; set; }
        public int pL3Q1 { get; set; }
        public int pL4Q1 { get; set; }
        public String path { get; set; }


        public int getNoOfBatch()
        {
            return noOfBatch;
        }

        public void setNoOfBatch(int noOfBatch)
        {
            this.noOfBatch = noOfBatch;
        }

        public String getOrderSeq()
        {
            return orderSeq;
        }

        public void setOrderSeq(String orderSeq)
        {
            this.orderSeq = orderSeq;
        }

        public int getOrderCtr()
        {
            return orderCtr;
        }

        public void setOrderCtr(int orderCtr)
        {
            this.orderCtr = orderCtr;
        }

        public MyPair[] getItemQtyArray()
        {
            return itemQtyArray;
        }

        public void setItemQtyArray(MyPair[] itemQtyArray)
        {
            this.itemQtyArray = itemQtyArray;
        }

        public Hashtable getBatchSize()
        {
            return batchSize;
        }

        public void setBatchSize(Hashtable batchSize)
        {
            this.batchSize = batchSize;
        }

        public int getpL1Q1()
        {
            return pL1Q1;
        }

        public void setpL1Q1(int pL1Q1)
        {
            this.pL1Q1 = pL1Q1;
        }

        public int getpL1Q2()
        {
            return pL1Q2;
        }

        public void setpL1Q2(int pL1Q2)
        {
            this.pL1Q2 = pL1Q2;
        }

        public int getpL2Q1()
        {
            return pL2Q1;
        }

        public void setpL2Q1(int pL2Q1)
        {
            this.pL2Q1 = pL2Q1;
        }

        public int getpL3Q1()
        {
            return pL3Q1;
        }

        public void setpL3Q1(int pL3Q1)
        {
            this.pL3Q1 = pL3Q1;
        }

        public int getpL4Q1()
        {
            return pL4Q1;
        }

        public void setpL4Q1(int pL4Q1)
        {
            this.pL4Q1 = pL4Q1;
        }

        public String getPath()
        {
            return path;
        }

        public void setPath(String path)
        {
            this.path = path;
        }


    }

}




